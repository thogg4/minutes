require 'mechanize'
require 'open-uri'
require 'pdf-reader'

base_url = 'http://syracuseut.com'

page = Mechanize.new.get("#{base_url}/YourGovernment/PlanningCommission/Minutes.aspx") do |page|
  page.links.select! { |link| link.href =~ /\.pdf/ }
end

pdfs = page.links.map { |link| link.href }

pdfs_with_string = []

pdfs.each do |pdf|
  pdf_contains = false

  begin
    pdf_url = URI.encode("#{base_url}#{pdf}")
    io = open(pdf_url)

    begin
      reader = PDF::Reader.new(io)
    rescue MalformedPDFError => e
      puts e
      puts pdf_url
    end

    reader.pages.each do |page|
      if page.text =~ /#{ARGV[0]}/
        pdfs_with_string << pdf
        puts page.text.gsub(ARGV[0], "\e[32m#{ARGV[0]}\e[0m")
        pdf_contains = true
      end
    end

    dot = "\e[31m.\e[0m"
    dot = "\e[32m.\e[0m" if pdf_contains
    print dot
  rescue OpenURI::HTTPError, SocketError => e
    puts e
    puts pdf_url
  end

end


puts ''
puts ''
puts 'pdf with your string:'
puts pdfs_with_string
